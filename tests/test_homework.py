import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from pages.login_page import LoginPage
from pages.project_page import ProjectPage

administrator_email = 'administrator@testarena.pl'


@pytest.fixture
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    login_page = LoginPage(browser)
    login_page.visit()
    login_page.login(administrator_email, 'sumXQQ72$L')

    yield browser
    browser.quit()


@pytest.fixture()
def project_page(browser):
    project_page = ProjectPage(browser)
    project_page.create_new_project()

    yield browser


def test_login(browser):
    user_email = browser.find_element(By.CSS_SELECTOR, '.user-info small').text
    assert user_email == administrator_email


def test_logout(browser):
    browser.find_element(By.CSS_SELECTOR, '.icons-switch').click()
    assert '/zaloguj' in browser.current_url
    assert browser.find_element(By.CSS_SELECTOR, '#password').is_displayed()


def test_create_new_project(project_page):
    new_project = project_page.find_element(By.CSS_SELECTOR, 'td:nth-child(1)')
    assert new_project.is_displayed()
