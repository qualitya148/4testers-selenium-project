import string
import random


def generate_random_string_with_prefix(prefix, length):
    random_string = ''.join(random.choices(string.ascii_letters + string.digits, k=length))
    return prefix + random_string
