from selenium.webdriver.common.by import By

class HomePage:

    def __init__(self, browser):
        self.browser = browser

    def verify_user_email(self, user_email):
        user_info = self.browser.find_element(By.CSS_SELECTOR, '.user-info small')
        assert user_info.text == user_email
