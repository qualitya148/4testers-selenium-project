from selenium.webdriver.common.by import By
from pages.random_str import generate_random_string_with_prefix


class ProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def create_new_project(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
        self.browser.find_element(By.CSS_SELECTOR, '.button_link_li').click()

        self.browser.find_element(By.ID, 'name').send_keys(generate_random_string_with_prefix("TR-", 10))
        self.browser.find_element(By.ID, 'prefix').send_keys(generate_random_string_with_prefix("TR",10))
        self.browser.find_element(By.ID, 'description').send_keys(generate_random_string_with_prefix("TR",10))
        self.browser.find_element(By.ID, 'save').click()

        self.browser.find_element(By.CSS_SELECTOR, ".item2").click()
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(
            self.browser.find_element(By.CSS_SELECTOR, 'td:nth-child(1)').text)
        self.browser.find_element(By.ID, "j_searchButton").click()